use backon::ConstantBuilder;
use backon::Retryable;
use clap::Parser;
use fork::{daemon, Fork};
use std::process::Command;
use std::time::Duration;
use zbus::{dbus_proxy, Connection};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Argument {
    #[arg(
        short,
        long,
        default_value_t = 0.5,
        help = "Delay between each retry in seconds"
    )]
    delay: f32,

    #[arg(short, long, default_value_t = 50, help = "Max retry times")]
    max_times: usize,

    #[arg(last = true, required = true, num_args=1..32, help = "Command to execute")]
    exec_arguments: Vec<String>,
}

#[dbus_proxy(
    interface = "org.kde.StatusNotifierWatcher",
    default_service = "org.kde.StatusNotifierWatcher",
    default_path = "/StatusNotifierWatcher"
)]
trait StatusNotifierWatcher {
    #[dbus_proxy(property)]
    fn is_status_notifier_host_registered(&self) -> zbus::Result<bool>;
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args = Argument::parse();

    let connection = Connection::session().await?;
    let constant_backoff = ConstantBuilder::default()
        .with_max_times(args.max_times)
        .with_delay(Duration::from_millis((args.delay * 1000.0) as u64));

    let retry_f = || async {
        let proxy = StatusNotifierWatcherProxy::new(&connection).await?;
        proxy.is_status_notifier_host_registered().await
    };

    retry_f.retry(&constant_backoff).await?;

    if let Ok(Fork::Child) = daemon(false, true) {
        // Exec the command
        Command::new(&args.exec_arguments[0])
            .args(&args.exec_arguments[1..])
            .spawn()?;
        std::process::exit(0);
    }
    std::process::exit(0);
}
