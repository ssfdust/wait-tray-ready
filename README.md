Wait until the tray is ready
==============================

When using tools like waybar or other status tools, you may encounter an issue where the icons of services such as CopyQ or Flameshot disappear from the system tray when Waybar takes longer to start up compared to these faster-starting tools. The only solution currently available is to restart CopyQ and Flameshot. This tool is specifically to solve this problem.

Usage
----------------

You need to prepend the tool to your startup command.
```bash
wait-tray-ready -- copyq
```
